<?php
// index.php
require_once('animal.php');
class Ape extends Animal{

    public $legs = 2; // 2
    public $cold_blooded = "no"; // "no"
    
    public function __construct($nama){
      $this->merk =$nama;
    }
   
    public function yell(){
        return "Yell : Auooo";
    }
  }

?>