<?php

require_once('animal.php');
class Frog extends Animal{

    public $legs = 4; // 4
    public $cold_blooded = "no"; // "no"
    
    public function __construct($nama){
      $this->merk =$nama;
    }
   
    public function Jump(){
        return "Jump : Hop Hop";
    }
  }

?>