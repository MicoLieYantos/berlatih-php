<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$kambing = new Animal("shaun");

echo "Nama :" . $kambing->merk . "<br>";
echo "legs :" . $kambing->legs . "<br>";
echo "cold blooded :" . $kambing->cold_blooded . "<br>" . "<br>";


$kera = new Ape("kera sakti");

echo "Nama :" . $kera->merk . "<br>";
echo "legs :" . $kera->legs . "<br>";
echo "cold blooded :" . $kera->cold_blooded . "<br>";
echo $kera-> yell() . "<br>" . "<br>";

$kodok =  new Frog("Buduk");

echo "Nama :" . $kodok->merk . "<br>";
echo "legs :" . $kodok->legs . "<br>";
echo "cold blooded :" . $kodok->cold_blooded . "<br>";
echo $kodok-> Jump();

